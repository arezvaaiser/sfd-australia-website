from fabric.api import env, task

from vps_deploy.django_fabric import transfer_files_git, update_nginx

env.user = 'deploy'
env.hosts = ['calais.sturm.com.au']
env.project_dir = '/srv/sfd'
env.site_name = 'sfd'
env.nginx_conf = 'deploy/nginx.conf'


@task
def deploy():
    """Install or deploy updates this website."""

    transfer_files_git()
    update_nginx()
